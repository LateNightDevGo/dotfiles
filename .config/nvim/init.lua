--set runtimepath^=~/.vim runtimepath+=~/.vim/after
--    let &packpath = &runtimepath
--    source ~/.vimrc
require('mymods')
vim.o.completeopt = "menu,menuone,noselect"
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

vim.opt.list = true
vim.opt.listchars = {
	tab = '»·',
	trail = '·'
}

vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true


local Plug = vim.fn['plug#']

vim.call('plug#begin', '~/.config/nvim/plugged')
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/nvim-compe'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'nvim-lua/plenary.nvim'
Plug('nvim-telescope/telescope.nvim', {tag = '0.1.1'} )
Plug('nvim-treesitter/nvim-treesitter', {['do'] = ':TSUpdate'})
Plug('tpope/vim-fugitive')
Plug 'nvim-lualine/lualine.nvim'
Plug 'nvim-tree/nvim-web-devicons'
vim.call('plug#end')

if vim.api.nvim_win_get_option(0, "diff") then
end

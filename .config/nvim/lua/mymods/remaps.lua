print("Remaps loaded")
-- map leader to <Space>
vim.keymap.set("n", " ", "<Nop>", { silent = true, remap = false })
vim.g.mapleader = ' '

vim.keymap.set("n", "<leader>bn", "<cmd>bnext<cr>", opts)

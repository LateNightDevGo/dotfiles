local lspc = require('lspconfig')
local lsp_capabilities = require('cmp_nvim_lsp').default_capabilities()

local default_setup = function(server)
	lspc[server].setup({})
end

require("mason").setup({
    ui = {
        icons = {
            package_installed = "✓",
            package_pending = "➜",
            package_uninstalled = "✗"
        }
    }
})
require('mason-lspconfig').setup({
	ensure_installed = {},
	handlers = {default_setup},
})

vim.api.nvim_set_hl(0, 'FloatBorder', {bg='#3B4252', fg='#5E81AC'})
vim.api.nvim_set_hl(0, 'NormalFloat', {bg='#3B4252'})
vim.api.nvim_set_hl(0, 'TelescopeNormal', {bg='#3B4252'})
vim.api.nvim_set_hl(0, 'TelescopeBorder', {bg='#3B4252'})
vim.api.nvim_set_hl(0, 'Pmenu', { ctermfg='White',  ctermbg='Black' })

local cmp = require('cmp')
cmp.setup({
	select = {behavior = cmp.SelectBehavior.Select},
	mapping = {
		['<C-p>'] = cmp.mapping.select_prev_item(select),
		['<C-n>'] = cmp.mapping.select_next_item(select),
		['<C-a>'] = cmp.mapping.confirm({ select = true }),
		["<C-Space>"] = cmp.mapping.complete(),
	},
	sources = {
		{ name = "nvim_lsp" },
		{ name = "cody" },
		{ name = "path" },
		{ name = "buffer" },
	}
})

local opts = {buffer = bufnr, remap = false}

vim.keymap.set("n", "<leader>gd", function() vim.lsp.buf.definition() end, opts)
vim.keymap.set("n", "<leader>gi", function() vim.lsp.buf.implementation() end, opts)
vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
vim.keymap.set("n", "<leader>ws", function() vim.lsp.buf.workspace_symbol() end, opts)
vim.keymap.set("n", "<leader>do", function() vim.diagnostic.open_float() end, opts)
vim.keymap.set("n", "<leader>dn", function() vim.diagnostic.goto_next() end, opts)
vim.keymap.set("n", "<leader>dp", function() vim.diagnostic.goto_prev() end, opts)
vim.keymap.set("n", "<leader>ca", function() vim.lsp.buf.code_action() end, opts)
vim.keymap.set("n", "<leader>rf", function() vim.lsp.buf.references() end, opts)
vim.keymap.set("n", "<leader>rn", function() vim.lsp.buf.rename() end, opts)
vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)

vim.diagnostic.config({
    virtual_text = true
})

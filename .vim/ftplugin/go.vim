"let g:ConqueTerm_Color = 2
"let g:ConqueTerm_CloseOnEnd = 1
"let g:ConqueTerm_StartMessages = 0
"
"function DebugSession()
"	silent make -o vimgdb -gcflags "-N -l"
"	redraw!
"	if (filereadable("vimgdb"))
"		call ConqueGdb vimgdb
"	else
"		echom "Couldn't find debug file"
"	endif
"endfunction
"
"function DebugSessionCleanup(term)
"	if (filereadable("vimgdb"))
"		let ds=delete("vimgdb")
"	endif
"endfunction
"
"call conque_term#register_function("after_close", "DebugSessionCleanup")
"nmap <F5> :call DebugSession()<CR>;

let g:tagbar_type_go = {
	\ 'ctagstype' : 'go',
	\ 'kinds'     : [
		\ 'p:package',
		\ 'i:imports:1',
		\ 'c:constants',
		\ 'v:variables',
		\ 't:types',
		\ 'n:interfaces',
		\ 'w:fields',
		\ 'e:embedded',
		\ 'm:methods',
		\ 'r:constructor',
		\ 'f:functions'
	\ ],
	\ 'sro' : '.',
	\ 'kind2scope' : {
		\ 't' : 'ctype',
		\ 'n' : 'ntype'
	\ },
	\ 'scope2kind' : {
		\ 'ctype' : 't',
		\ 'ntype' : 'n'
	\ },
	\ 'ctagsbin'  : 'gotags',
	\ 'ctagsargs' : '-sort -silent'
\ }

set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'ycm-core/YouCompleteMe'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-fugitive'
Plugin 'fatih/vim-go'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Plugin 'vim-syntastic/syntastic'
call vundle#end()

autocmd InsertLeave,WinEnter * setlocal foldmethod=syntax
autocmd InsertEnter,WinLeave * setlocal foldmethod=manual

set mouse=a

syntax on
filetype plugin indent on
colors monokai-phoenix
set ts=8
set sw=8
au Filetype python setl et ts=4 sw=4
set list listchars=tab:»·,trail:·
set tags=tags
set cursorline

if has('vim')
	set noesckeys
endif

set smartindent
set autoindent
set spelllang=en_us,sv
set timeoutlen=100
nnoremap <F6> :set spell!<CR>

"---------------------------------
"Vim config files
"--------------------------------
source ~/.vim/functions.vim

"---------------------------------
"Custom key mapping
"---------------------------------
map <Up> <Nop>
map <Down> <Nop>
map <Left> <Nop>
map <Right> <Nop>
imap <Up> <Nop>
imap <Down> <Nop>
imap <Left> <Nop>
imap <Right> <Nop>

map <Space> <Leader>
let mapleader =" "

nnoremap <C-h> <C-w><C-h>
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>

nnoremap <Leader>x i
"update syntax highlighting
nnoremap <Leader><F9>:syntax sync fromstart<CR>
set showcmd
imap <C-J> <Plug>snipMateNextOrTrigger<CR>
nmap <F8> :TagbarToggle<CR>

nnoremap <buffer> <silent> <Leader>gd :YcmCompleter GoTo<CR>
nnoremap <buffer> <silent> <Leader>gr :YcmCompleter GoToReferences<CR>
nnoremap <buffer> <silent> <Leader>rr :YcmCompleter RefactorRename<space>

"---------------------------------
"Formatting
"---------------------------------
set encoding=utf-8
set fileencoding=utf-8
set rnu
set nu

"---------------------------------
" Go
"---------------------------------
let $GOPATH="/home/david/Utveckling/Go"

"---------------------------------
"Airline
"---------------------------------
set laststatus=2
let g:airline_detect_spell=1
let g:airline_detect_modified=1
let g:airline_theme='badwolf'
let g:airline_symbols = {}
let g:airline_symbols.branch = "⎇ "
let g:airline#extensions#tagbar#enabled = 1

"---------------------------------
"Syntastic
"---------------------------------
let g:syntastic_go_checkers = ['govet', 'errcheck', 'go']

"---------------------------------
"Go Debugging
"---------------------------------
map <C-n> :NERDTreeToggle<CR>

"
"Go Debugging
"---------------------------------

nmap <Leader>b :GoToggleBreakpoint<CR>
nmap <Leader>d :GoDebug<CR>
"nmap <leader>? :GoDebugTest
